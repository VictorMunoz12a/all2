package com.softtek.academy.spring.dao;

import org.springframework.jdbc.core.JdbcTemplate;

import com.softtek.academy.spring.beans.Person;

public class StudentDao {
	private JdbcTemplate jdbcTemplate;  
	  
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {  
	    this.jdbcTemplate = jdbcTemplate;  
	}  
	public int saveStudent(Person e){  
	    String query="insert into Person values('"+e.getName()+"','"+e.getAge()+"','"+e.getId()+"')";  
	    return jdbcTemplate.update(query);
	}
}
