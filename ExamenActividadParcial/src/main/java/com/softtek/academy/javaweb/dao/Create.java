package com.softtek.academy.javaweb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.softtek.academy.javaweb.model.Conexion;

public class Create {
	 public int createTodoTaks(String list, int done) {
	        try  {
	        	Connection db = Conexion.getInstance().getConnection();
	        	PreparedStatement ps = db.prepareStatement("INSERT INTO to_do_list (list, is_done) values (?, ?)",
	                    Statement.RETURN_GENERATED_KEYS);
	            ps.setString(1, list);
	            ps.setInt(2, done);
	            ps.executeQuery();
	            ResultSet rs = ps.getGeneratedKeys();
	            if (rs.next()) {
	                return  rs.getInt(1);
	            }
	        } catch (Exception e) {
	            e.printStackTrace();
	            return 0;
	        }
	        return 0;
	    }

}
