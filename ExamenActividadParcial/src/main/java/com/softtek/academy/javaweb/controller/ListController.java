package com.softtek.academy.javaweb.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.model.Model;
import com.softtek.academy.javaweb.service.service;

public class ListController extends HttpServlet{
	 public ListController() {
	        super();
	    }

	    @Override
	    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {

	        List<Model> list = new ArrayList<Model>();
	        service todoListService = new service();
	        String done = request.getParameter("done");
	        if (done != null && !done.isEmpty()) { 
	            list = todoListService.getAllList(Integer.parseInt(done), false);
	        } else {
	            list = todoListService.getAllList(0, true);
	        }
	        RequestDispatcher rd = request.getRequestDispatcher("/views/List.jsp");
	        request.setAttribute("list", list);
	        rd.forward(request, response);
	    }

}
