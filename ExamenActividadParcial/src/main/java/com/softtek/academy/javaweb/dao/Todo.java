package com.softtek.academy.javaweb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.softtek.academy.javaweb.model.Conexion;
import com.softtek.academy.javaweb.model.Model;

public class Todo {

	 public List<Model> getList(int done, Boolean all) {
	        List<Model> list = new ArrayList<Model>();
	        try  {
	        	Connection db = Conexion.getInstance().getConnection();
	        	ResultSet rs = null;
	            if (all) {
	                rs = db.prepareStatement("SELECT * FROM TO_DO_LIST").executeQuery();
	            } else {
	                PreparedStatement ps = db.prepareStatement("SELECT * FROM TO_DO_LIST WHERE is_done = ?");
	                ps.setInt(1, done);
	                rs = ps.executeQuery();
	            }
	            while (rs.next()) {
	                list.add(new Model(rs.getInt(1), rs.getString(2), rs.getInt(3)));
	            }
	            return list;
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        }
	    }

}
