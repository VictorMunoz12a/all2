package com.softtek.academy.javaweb.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.model.Model;
import com.softtek.academy.javaweb.service.service;

public class Controller  extends HttpServlet {

    private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Controller() {
        super();
        // TODO Auto-generated constructor stub
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       String list = request.getParameter("list");
        Boolean is_done = Boolean.valueOf(request.getParameter("done"));
        service todoService = new service();
        int created = todoService.createTodo(list, is_done  == false ? 0 : 1);
        service todoListService = new service();
        if(created == 0){
            List<Model> listItem = todoListService.getAllList(0, true);
            RequestDispatcher rd = request.getRequestDispatcher("/views/TodoList.jsp");
            request.setAttribute("list", listItem);
            rd.forward(request, response);
        } else {
            RequestDispatcher rd = request.getRequestDispatcher("/views/Error.jsp");
            rd.forward(request, response);
        }
    }
}
