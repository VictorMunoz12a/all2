package com.softtek.academy.javaweb.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.model.Model;
import com.softtek.academy.javaweb.service.service;

public class UpdateController extends  HttpServlet{
	 public UpdateController() {
	        super();
	    }

	    @Override
	    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	        service todoListService = new service();
	        Boolean updated = todoListService.UpdateList(Integer.parseInt(request.getParameter("id")),
	                Integer.parseInt(request.getParameter("done")));
	        if(updated){
	            List<Model> list = todoListService.getAllList(0 ,true);
	            RequestDispatcher rd = request.getRequestDispatcher("/views/List.jsp");
	            request.setAttribute("list", list);
	            rd.forward(request, response);
	        }else {
	        	System.out.println("error");
	            /*RequestDispatcher rd = request.getRequestDispatcher("/views/Error.jsp");
	            rd.forward(request, response);*/
	        }
	    }

}
