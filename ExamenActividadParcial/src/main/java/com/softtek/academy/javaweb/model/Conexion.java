package com.softtek.academy.javaweb.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {
	
	  private static Conexion instance;
		private Connection connection;
		private String url = "jdbc:mysql://localhost:3306/servlets1?serverTimezone=UTC#";
		private String username = "root";
		private String password = "1234";

		private Conexion() throws SQLException {
			try {
				Class.forName("com.mysql.jdbc.Driver");
				this.connection = DriverManager.getConnection(url, username, password);
			} catch (ClassNotFoundException ex) {
				System.out.println("Database Connection Creation Failed : " + ex.getMessage());
			}
		}

		public Connection getConnection() {
			return connection;
		}

		public static Conexion getInstance() throws SQLException {
			if (instance == null) {
				instance = new Conexion();
			} else {
				return instance;
			}
			return instance;

		}
}
