package com.softtek.academy.spring.beans;

public class DIConstructor {
private SpellChecker spellChecker;
public DIConstructor(SpellChecker spellChecker) {
	System.out.println("Inside Text Editor constructor");
	this.spellChecker=spellChecker;
	
}
public void spellCheck() {
	spellChecker.checkSpelling();
}
}
