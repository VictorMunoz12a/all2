package com.softtek.academy.spring.beans;

public class SpellChecker {
	public SpellChecker() {
		System.out.println("Spell checker constructor.");
	}

	public void checkSpelling() {
		System.out.println("Inside checkSpelling");
	}
}
