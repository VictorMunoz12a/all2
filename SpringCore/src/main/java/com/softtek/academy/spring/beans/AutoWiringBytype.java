package com.softtek.academy.spring.beans;

public class AutoWiringBytype {
	private String name;

	public void getName() {
		System.out.println("Por tipo "+name);
	}

	public void setName(String name) {
		this.name = name;
	}
}
