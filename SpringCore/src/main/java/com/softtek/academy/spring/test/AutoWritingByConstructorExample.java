package com.softtek.academy.spring.test;

import com.softtek.academy.spring.beans.AutoWiringAnnotationConstructor;

public class AutoWritingByConstructorExample {
	public static void main(String[] args) {
		AutoWiringAnnotationConstructor te = (AutoWiringAnnotationConstructor)
				MyApplicationContextClass.context.getBean("autoWiringAnnotationConstructor");
		te.getSpellChecker();
	}
}
