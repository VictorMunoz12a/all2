package com.softtek.academy.spring.test;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.softtek.academy.spring.beans.BeanLifeCycleExample;


public class LifeCycleTest {

	public static void main(String[] args) {
		AbstractApplicationContext context= new ClassPathXmlApplicationContext("\"com/softtek/academy/spring/resources/applicationcontext.xml\"");
		// TODO Auto-generated method stub
		BeanLifeCycleExample he= (BeanLifeCycleExample) MyApplicationContextClass.context.getBean("beanLifeCyle");
		he.getMessage();
		context.registerShutdownHook();
	}

}
