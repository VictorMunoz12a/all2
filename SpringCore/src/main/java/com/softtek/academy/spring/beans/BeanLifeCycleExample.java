package com.softtek.academy.spring.beans;

public class BeanLifeCycleExample {
private String message;

public void getMessage() {
	System.out.println("Your Message: "+ message);
}

public void setMessage(String message1) {
	this.message = message1;
}
public void init () {
	System.out.println("Bean is going through init.");
}
public void destroy() {
	System.out.println("Bean will be destroyed now");
}
}
