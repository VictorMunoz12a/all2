package com.softtek.academy.spring.test;

import com.softtek.academy.spring.beans.ChildBean;
import com.softtek.academy.spring.beans.ParentBean;

public class InheritanceTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ParentBean obA = (ParentBean) MyApplicationContextClass.context.getBean("parentBean");
		obA.getMessage1();
		obA.getMessage2();
		ChildBean obB = (ChildBean) MyApplicationContextClass.context.getBean("childBean");
		obB.getMessage1();
		obB.getMessage2();
	}
}


