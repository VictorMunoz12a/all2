package com.softtek.academy.spring.beans;
import com.softtek.academy.spring.beans.SpellChecker;
public class AutoWiringAnnotationConstructor {
private SpellChecker spellChecker;

public void  getSpellChecker() {
	System.out.println("esto es lo que va a  imprimir: "+ spellChecker);
}

public void setSpellChecker(SpellChecker spellChecker) {
	this.spellChecker = spellChecker;
}

public void spellCheck() {
	spellChecker.checkSpelling();
}

}
