package com.softtek.academy.spring.test;

import org.springframework.beans.factory.annotation.Autowired;

import com.softtek.academy.spring.beans.SpellChecker;


public class AutoWiringAnnotationSetterMethod {
private SpellChecker spellChecker;

public SpellChecker getSpellChecker() {
	return spellChecker;
}

public void setSpellChecker(SpellChecker spellChecker) {
	this.spellChecker = spellChecker;
}

public void spellCheck() {
	spellChecker.checkSpelling();
}

}
