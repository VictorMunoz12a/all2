package com.softtek.academy.spring.beans;

public class AutoWiringByname {
	private SpellChecker spellChecker;
	private String name;

	public SpellChecker getSpellChecker() {
		return spellChecker;
	}

	public void getName() {
		System.out.println(name);
	}

	public void setSpellChecker(SpellChecker spellChecker) {
		this.spellChecker = spellChecker;
	}

	public void setName(String name1) {
		this.name = name1;
	}

	public void spellCheck() {
		spellChecker.checkSpelling();
	}
}
