package com.softtek.academy.spring.beans;

public class DISetter {

	private SpellChecker spellChecker;
	
	public void setSpellChecker(SpellChecker spellChecker) {
		System.out.println("Inside setSpellChecker: ");
		this.spellChecker=spellChecker;
	}
	public SpellChecker getSpellChecker() {
		return spellChecker;
	}
	public void spellChecker() {
		spellChecker.checkSpelling();
	}
}
