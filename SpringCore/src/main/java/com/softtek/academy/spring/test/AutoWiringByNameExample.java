package com.softtek.academy.spring.test;

import com.softtek.academy.spring.beans.AutoWiringByname;

public class AutoWiringByNameExample {

	public static void main(String[] args) {
		AutoWiringByname te = (AutoWiringByname) MyApplicationContextClass.context.getBean("autoWiringByname");
		te.spellCheck();
		te.getName();

	}

}
