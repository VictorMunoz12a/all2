package com.softtek.academy.spring.test;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MyApplicationContextClass implements InitializingBean {
	protected static ApplicationContext context = new ClassPathXmlApplicationContext(
			"com/softtek/academy/spring/resources/applicationcontext.xml");

	public void afterPropertiesSet() throws Exception {

	}
}
