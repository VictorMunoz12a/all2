package com.softtek.academy.javweb.servlet;

public class WelcomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String message;

	public WelcomeServlet() {
		super();

	}

	public void init() throws ServletException {
		message = "Soy el neto gonzalez";

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<h1>" + message + "</h1>");

		String title = "Using GET method to read from data";
		String fName = request.getParameter("first_name");
		String lName = request.getParameter("last_name");
		String docType = "<!doctype html public \"//w3c//dtd html 4.8 transitional//en\">\n";
		out.print(docType + "<html>\n" + "<head><title>" + title + "</title></head>" + "<body bgcolor=\"728dba\">\n"
				+ "<h1 align=\"center\">" + title + "</h1>\n" + "<ul>\n" + "<li><b>First Name<\b>" + fName + "\n"
				+ "<li><b>Last Name</b>" + lName + "\n" + "</ul>\n" + "</body>" + "</html>");
	}

}