package com.softtek.academy.javaweb.model;

public class Controller {

	private String list;
	private int is_done;
	
	public Controller(String list, int is_done) {
		super();
		
		this.list = list;
		this.is_done = is_done;
	}
	
	
	public String getList() {
		return list;
	}
	public void setList(String list) {
		this.list = list;
	}
	public int isIs_done() {
		return is_done;
	}
	public void setIs_done(int is_done) {
		this.is_done = is_done;
	}
}
