package com.softtek.academy.javaweb.view;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.model.Controller;
import com.softtek.academy.javaweb.model.Model;
@WebServlet("/Request")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public Register() {
    }



 protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
 
	 
  String acti=request.getParameter("list");
  String is_Done=request.getParameter("is_done");
  
  int is_d=Integer.parseInt(is_Done);
  
 
  if(acti=="tareas"){
   Controller busuario=new Controller(acti,is_d);//convertir los valors declarados arriba a valores establecidos en la otra clase
   boolean sw=Model.agregarUsuario(busuario);
   if(sw){
    request.getRequestDispatcher("/JSP/Message.jsp").forward(request, response);
   }else{
    PrintWriter out=response.getWriter();
    out.println("Si estas viendo este mensaje es por que algo salio mal, no se pudo completar tu solicitud.");
   }
  }
 }
}
